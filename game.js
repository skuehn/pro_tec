var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

var game = new Phaser.Game(config);

function preload ()
{
    this.load.image('grass', 'images/grass.jpg');
    this.load.image('wall', 'images/wall_vs2.png');
    this.load.image('base', 'images/mine.jpg');
    this.load.image('bullet', 'images/bullet.png');
    this.load.image('enemy', 'images/enemy.png');
    this.load.spritesheet('dude', 
        'src/games/firstgame/assets/dude.png',
        { frameWidth: 32, frameHeight: 48 }
    );
}

function create ()
{
    this.add.tileSprite(400, 300, 800, 600, 'grass');
	this.add.image(400, 300, 'base')
	game.time.events.repeat(Phaser.Timer.SECOND * 2, 10, spawnEnemy, this);
}

function spawnEnemy () {
    var Enemy = game.add.sprite(game.world.randomX, 0, 'enemy');
}

function update ()
{
}